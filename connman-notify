#!/usr/bin/env python3
# connman-notify: desktop notification integration for connman
# Copyright(c) 2014-2019 by wave++ "Yuri D'Elia" <wavexx@thregr.org>
# Distributed under GPL2 (see COPYING) WITHOUT ANY WARRANTY.
from __future__ import print_function, division, generators, unicode_literals

import argparse

import gi
from gi.repository import GLib as glib

import dbus
import dbus.mainloop.glib


# Constants
APP_NAME  = 'connman'
SHORT_DEV = True
SHOW_ADDR = True

PROTO_LST = ['IPv4', 'IPv6']
STATE_IGN = ['ready', 'idle']
STATE_MAP = {'disconnect': 'disconnected',
             'association': 'associating ...',
             'configuration': 'configuring ...'}

# Nifty globals
NOTIFY_IF = None


def refresh_notify_if(bus):
    global NOTIFY_IF
    NOTIFY_IF = dbus.Interface(bus.get_object('org.freedesktop.Notifications',
                                              '/org/freedesktop/Notifications'),
                               'org.freedesktop.Notifications')


def notify(title, text, time=1000):
    if NOTIFY_IF:
        NOTIFY_IF.Notify(APP_NAME, 0, '', title, text, '', '', time)


def property_changed(bus, manager, name, value, path):
    # ignore global state changes
    if path == '/': return

    # shorten device names
    srv_name = path[path.rfind("/") + 1:]
    if SHORT_DEV:
        srv_name = srv_name[0:srv_name.find("_")]

    # interface state changes
    if name == 'State' and value not in STATE_IGN:
        state = STATE_MAP.get(value, value)
        srv_obj = dbus.Interface(bus.get_object('net.connman', path), 'net.connman.Service')
        srv_prop = None
        try:
            srv_prop = srv_obj.GetProperties()
            if 'Name' in srv_prop:
                srv_name += "/" + srv_prop['Name']
            if SHOW_ADDR and value == 'online':
                addrs = []
                for proto in PROTO_LST:
                    if proto in srv_prop and 'Address' in srv_prop[proto]:
                        addrs.append(str(srv_prop[proto]['Address']))
                if addrs:
                    state += " [" + ", ".join(addrs) + "]"
        except dbus.DBusException:
            pass
        notify(srv_name, state)

    # IPv4/6 changes
    if SHOW_ADDR and name in PROTO_LST and 'Address' in value:
        srv_obj = dbus.Interface(bus.get_object('net.connman', path), 'net.connman.Service')
        srv_prop = srv_obj.GetProperties()

        # only report new addresses
        if srv_prop['State'] == 'online':
            if 'Name' in srv_prop:
                srv_name += "/" + srv_prop['Name']
            notify(srv_name, str(value['Address']))


def name_owner_changed(name, old_owner, new_owner, path):
    if name == 'org.freedesktop.Notifications':
        refresh_notify_if(dbus.SessionBus())


if __name__ == '__main__':
    ap = argparse.ArgumentParser(description='connman notification daemon')
    ap.add_argument('-l', dest="long_dev", action="store_true",
                    help='Show long interface names')
    ap.add_argument('-n', dest="hide_addr", action="store_true",
                    help='Hide assigned IPv4/6 addresses')
    args = ap.parse_args()

    if args.long_dev: SHORT_DEV = False
    if args.hide_addr: SHOW_ADDR = False

    # listen on system-wide bus for connman events
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    bus = dbus.SystemBus()
    manager = dbus.Interface(bus.get_object("net.connman", "/"), "net.connman.Manager")

    bus.add_signal_receiver(lambda name, value, path: property_changed(bus, manager, name, value, path),
                            bus_name="net.connman",
                            signal_name="PropertyChanged",
                            path_keyword="path")

    # register on session bus for notification daemon changes
    sessionbus = dbus.SessionBus()
    sessionbus.add_signal_receiver(name_owner_changed,
                                   bus_name='org.freedesktop.DBus',
                                   signal_name='NameOwnerChanged',
                                   path_keyword='path')
    refresh_notify_if(sessionbus)

    # main loop
    mainloop = glib.MainLoop()
    mainloop.run()
